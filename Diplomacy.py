#!/usr/bin/env python3

# ---------------------------
# projects/collatz/Collatz.py
# Copyright (C)
# Glenn P. Downing
# ---------------------------

class Army:
    def __init__(self, armyName, armyLocation, armyTarget = None, armyPoints = 0, isFighting =  False):
        self.armyName = armyName
        self.armyLocation = armyLocation
        self.armyTarget = armyTarget
        self.armyPoints = armyPoints
        self.isFighting = isFighting

# -------------
# diplomacy_solve
# -------------

def diplomacy_solve(r, w):
    """
    r a reader
    w a writer
    """
    armies = []

    for s in r:
        # Reading string s, grabs info important to that army according army's action and appends army to armies list.
        # for example: a = A-0 Madrid-1 Support-2 B-3
        a = s.split()
        if a[2] == "Move":
            armies.append(Army(a[0], a[3])) # Move -> armyName and armyTarget.
        elif a[2] == "Support":
            armies.append(Army(a[0], a[1], a[3])) # Support -> armyName, armyLocation, and armyTarget.
        # Got rid of this elif line because it was suggested by TA for coverage purposes.
        # elif a[2] == "Hold":
        #    army = Army(a[0], a[1]) # Hold -> armyName, armyLocation
        else:
            armies.append(Army(a[0], a[1])) # Hold -> armyName, armyLocation

    fightLocation = []
    totalArmies = len(armies)
    for i in range(totalArmies):
        army = armies[i]   # army = A Madrid
        if army.isFighting:
            continue
        #fight = []
        #for i in army:
        #    fight.append(i)
        fight = [army]     # fight = [A Madrid]
        nextArmy = i + 1   # nextArmy = B Madrid
        while totalArmies > nextArmy:
            if armies[nextArmy].armyLocation == army.armyLocation: # A Madrid, B Madrid, so True.
                army.isFighting, armies[nextArmy].isFighting  = True, True
                fight.append(armies[nextArmy])  # fight = [A Madrid, B Madrid]
            nextArmy += 1 # nextArmy = C London B
        totalFights = len(fight)
        if totalFights > 1:
            fightLocation.append(fight) # fightLocation = [[A Madrid, B Madrid], [...]]
    results = []
    for army in armies:
        if army.isFighting != True:
            results.append(army.armyName + " " + army.armyLocation) # if army is not fighting, add to results list.
    for army in armies:
        if (not army.armyTarget == None) & (army.isFighting == False):
            for nextArmy in armies:
                if army.armyTarget == nextArmy.armyName: # if army support target is the name of nextArmy, then nextArmy gains 1 point.
                    nextArmy.armyPoints += 1 # 1 extra point to nextArmy for neing the target (support) of army.
    afterFight = []
    for fight in fightLocation:
        maxPoints = 0
        while len(fight) > 1: # Armies that are fighting.
            i = len(fight) - 1
            while i >= 0:
                if fight[i].armyPoints <= maxPoints:
                    afterFight.append(fight[i].armyName + " [dead]") # if armyPoints is less than maxPoints then army is dead and appended to afterFight list.
                    fight.pop(i) # pops index -1 (1st army) of fight list.
                i = i - 1 # traverses through the fight list.
            maxPoints += 1
        if len(fight) >= 1:
            afterFight.append(fight[0].armyName + " " + fight[0].armyLocation) # all of the surviving armies get appended to afterFight list.
    results.extend(afterFight)
    # write the results of the diplomacy game.
    sortedResults = sorted(results) # sort the results aalphabetically.

    for army in sortedResults: # write results of armies after fights.
        w.write(army)
        w.write("\n")