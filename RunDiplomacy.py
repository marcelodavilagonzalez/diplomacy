#!/usr/bin/env python3

# ------------------------------
# projects/collatz/RunCollatz.py
# Copyright (C)
# Glenn P. Downing
# ------------------------------

# -------
# imports
# -------

import sys

from Diplomacy import diplomacy_solve

# ----
# main
# ----

if __name__ == "__main__":
    diplomacy_solve(sys.stdin, sys.stdout)

""" #pragma: no cover
$ cat RunDiplomacy1.in
A Madrid Hold
B Barcelona Move Madrid
C London Support B
D Austin Move London



$ python RunDiplomacy.py < RunDiplomacy1.in > RunDiplomacy1.out

$ cat RunDiplomacy1.out
A [dead]
B [dead]
C [dead]
D [dead]



$ python -m pydoc -w Diplomacy
# That creates the file Diplomacy.html
"""